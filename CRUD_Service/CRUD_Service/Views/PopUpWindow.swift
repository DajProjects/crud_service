//
//  PopUpWindow.swift
//  CRUD_Service
//
//  Created by Daniel Abad Jaraute on 15/02/2022.
//

import SwiftUI

struct PopUpUserWindow: View {
    @EnvironmentObject var env : CRUDViewBindings
    
    var callback: ()-> Void = {}
    
    var body: some View {
        GeometryReader{ geometry in
            ZStack {
                if env.showPopup {
                    // PopUp window content
                    Color.black.opacity(env.showPopup ? 0.3 : 0)
                        .edgesIgnoringSafeArea(.all)
                }
            }
            .overlay(popupContent())
        }
        .onTapGesture{
            self.hideKeyboard();
        }
    }
    
    func popupContent() -> some View {        
        
        let bindingName = Binding<String>(get: {
            if let user = env.selectedUser {
                return user.name;
            }
            else
            {
                return env.newUser.name;
            }
        }, set: {
            if (env.selectedUser != nil){
                self.env.selectedUser!.name = $0
            }
            else
            {
                env.newUser.name = $0
            }
        })
        let bindingDate = Binding<Date>(get: {
            if let user = env.selectedUser {
                return user.birthDate;
            }
            else
            {
                return env.newUser.birthDate
            }
        }, set : {
            if (env.selectedUser != nil){
                
                self.env.selectedUser!.birthdate = Utils.getCustomDateF("yyyy-MM-dd'T'12:00:00").string(from: $0)
            }
            else
            {
                env.newUser.birthdate = Utils.getCustomDateF("yyyy-MM-dd'T'12:00:00").string(from: $0)
            }
        })
        
        return GeometryReader{ geometry in
            ZStack{
                if env.showPopup {
                    Spacer()
                    VStack(alignment: .center, spacing: 0) {
                        HStack{
                            Spacer()
                            Button(action: {
                                hideKeyboard()
                                env.showPopup = false;
                            }){
                                Image("close")
                                    .resizable()
                                    .foregroundColor(.black)
                                    .padding(.top, 6)
                                    .padding(.trailing, 6)
                            }
                            .frame(width: 40, height: 40, alignment: .center)
                        }
                        .padding(.bottom, -20)
                        HStack{
                            Text(LocalizedStringKey("NAME"))
                                .font(.title2)
                             .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                            Spacer()
                        }
                        .padding(.horizontal, 16)
                        .padding(.vertical, 6)
                        HStack{
                            TextField(bindingName.wrappedValue, text: bindingName)
                                .textFieldStyle(RoundedBorderTextFieldStyle())
                                .font(.title3)
                        }
                        .padding(.horizontal, 16)
                        HStack{
                            Text(LocalizedStringKey("YEAR"))
                                .font(.title2)
                             .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                            Spacer()
                        }
                        .padding(.horizontal, 16)
                        .padding(.top, 12)
                        .padding(.bottom, 6)
                        HStack{
                            DatePicker(Utils.getCustomDateF().string(from: bindingDate.wrappedValue), selection: bindingDate,
                                in: ...Date(),
                                displayedComponents: .date)
                        }
                        .padding(.horizontal, 16)
                        Spacer()
                        HStack{
                            Button(action: {
                                self.hideKeyboard()
                                if (bindingName.wrappedValue != ""){
                                    env.showLoader = true;
                                    if (env.selectedUser == nil){
                                        // Creación de usuario
                                        APIController.post(from: EndPoints.user, item: User(id: 0, name: bindingName.wrappedValue, birthdate: Utils.getCustomDateF("yyyy-MM-dd'T'12:00:00").string(from: bindingDate.wrappedValue))) { (result) in
                                            env.showLoader = false;
                                            
                                            switch result {
                                            case .success(_):
                                                self.env.showPopup = false;
                                                self.callback();
                                            case let .failure(error):
                                                print(error);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        // Edición de usuario
                                        APIController.put(from: EndPoints.user, item: env.selectedUser!) { (result) in
                                            env.showLoader = false;
                                            
                                            switch result {
                                            case .success(_):
                                                self.env.showPopup = false;
                                                self.callback();
                                            case let .failure(error):
                                                print(error);
                                            }
                                        }
                                        
                                    }
                                }
                            }, label: {
                                Text(env.selectedUser == nil ? LocalizedStringKey("CREATE") : LocalizedStringKey("EDIT"))
                                    .font(.title2)
                                    .foregroundColor(.white)
                                    .fontWeight(.semibold)
                            })
                            .padding(2)
                            .frame(maxWidth: .infinity, maxHeight: .infinity)
                        }
                        .background(Color.green)
                        .cornerRadius(6)
                        .padding(.bottom, 10)
                        .frame(width: geometry.size.width * 0.5, height: 40)
                    }
                    .background(Color.white)
                    .clipShape(RoundedRectangle(cornerRadius: 6))
                    .overlay(
                        RoundedRectangle(cornerRadius: 6)
                            .stroke(Color.black, lineWidth: 2)
                    )
                    .animation(.easeIn)
                    .transition(.offset(x: 0, y: geometry.belowScreenEdge))
                    .frame(width: geometry.size.width * 0.9, height: geometry.size.height * 0.3, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    Spacer()
                }
                //LoaderView(show: $showLoader)
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
        }
    }
}

private extension GeometryProxy {
    var belowScreenEdge: CGFloat {
        UIScreen.main.bounds.height - frame(in: .global).minY
    }
}

struct PopUpWindow_Previews: PreviewProvider {
    
    static var previews: some View {
        PopUpUserWindow(callback: {})
    }
}
