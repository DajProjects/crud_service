//
//  CRUDView.swift
//  CRUD_Service
//
//  Created by Daniel Abad Jaraute on 12/02/2022.
//

import SwiftUI

struct CRUDView: View {
    @StateObject private var userController = ObservableFilteredList<User>()
    @State private var usersFilter = Filter()
    
    //@State private var showPopup: Bool = false
    @EnvironmentObject var bindings : CRUDViewBindings
    //@State private var showLoader: Bool = false
    //@State private var selectedItem: User?

    func loadUsers(){
        APIController.get(from: EndPoints.user, type: User.self) { (result) in
            switch result{
            case let .success(users):
                userController.setList(list: users, filter: nil)
            case let .failure(error):
                print(error)
            }
        }
    }
    
    var body: some View {
        ZStack{
            VStack(alignment: .leading){
                HStack{
                    Text(LocalizedStringKey("USERS"))
                        .font(.title)
                        .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                    Spacer()
                    
//                    Button(action: {
//                        bindings.showFilters.toggle()
//                    })
//                    {
//                        Image("filter")
//                            .resizable()
//                            .padding(.vertical, 12)
//                            .padding(.horizontal, 10)
//                    }
//                    .frame(width: 40, height: 40, alignment: .center)
//                    .clipShape(Circle())
//                    .overlay(Circle()
//                                .stroke(Color.black, lineWidth: 2)
//                    )
                }
                .padding(.horizontal, 16)
                Divider()
                //UserListItem(item: nil){};
                List(userController.filteredList){ user in
                    UserListItem(item: user, modifiedCallback: {
                        loadUsers();
                    })
                    .environmentObject(bindings)
                    .padding(0)
                }
                .environment(\.defaultMinListRowHeight, 10)
                .padding(0)
                Spacer()
                    .frame(height: 50)
            }
            .padding(0)
            .onAppear {
                loadUsers();
            }
            
            // Button ADD
            VStack {
                Spacer()
                HStack {
                    Spacer()
                    Button(action: {
                        bindings.newUser = User();
                        bindings.selectedUser = nil;
                        bindings.showPopup.toggle()
                        //userController.filter(filter: nil)
                    }, label: {
                        Text("+")
                            .font(.system(.largeTitle))
                            .frame(width: 60, height: 60)
                            .foregroundColor(Color.white)
                            .padding(.bottom, 0)
                    })
                    .background(Color.blue)
                    .cornerRadius(38.5)
                    .padding(.trailing, 8)
                    .shadow(color: Color.black.opacity(0.3),
                            radius: 3,
                            x: 3,
                            y: 3)
                }
            }
            
            // Edition Popup
            PopUpUserWindow(){
                loadUsers();
            }
                .environmentObject(bindings)
            
            // FilterPopUp
            ZStack(alignment: .trailing){
                if bindings.showFilters{
                    VStack{
                        Spacer()
                            .frame(height: 50)
                        Color.black.opacity(0.3)
                            .edgesIgnoringSafeArea(.all)
                    }
                    .onTapGesture{
                        bindings.showFilters = false
                    }
                    GeometryReader{ geometry in
                        VStack{
                            Spacer()
                                .frame(idealHeight: 50)
                                .fixedSize()
                            HStack{
                                Spacer()
                                VStack{
                                    HStack{
                                        Text(LocalizedStringKey("NAME"))
                                            .font(.title2)
                                            .fontWeight(.semibold)
                                        Spacer()
                                        Menu {
                                            ForEach(FilterElem.getStringModes(), id: \.self){ m in
                                                Button(action: {
                                                    
                                                }){
                                                    Text(LocalizedStringKey(m.rawValue))
                                                }
                                                
                                            }
                                            
                                        } label: {
                                             Text(LocalizedStringKey("FM_MODE"))
                                             Image(systemName: "square.and.pencil")
                                        }
                                    }
                                    
                                }
                                .padding()
                                .frame(width: geometry.size.width * 0.7, height: geometry.size.height * 0.8, alignment: .topLeading)
                                .background(Color.white)
                                .cornerRadius(6)
                            }
                        }
                        .padding(0.0)
                    }
                    .animation(.easeIn)
                    .transition(.offset(x: 600, y: 0))
                }
            }
            // Loader
            LoaderView()
                .environmentObject(bindings)
        }
        .padding(0)
    }
}

struct UserListItem: View {
    var item: User?;
    @EnvironmentObject var parentBindings: CRUDViewBindings
    var modifiedCallback: () -> Void
    @State private var showDeleteAlert = false
    
    var pad: CGFloat = 8
    
//    init(item: User?, showLoader: Binding>){
//        self.item = item;
//    }
    
    var body: some View {
        GeometryReader { metrics in
            HStack(alignment: .center, spacing: 0){
                VStack(alignment: .leading, spacing: 0){
                    HStack(spacing: 0){
                        Text(LocalizedStringKey("NAME"))
                            .fontWeight(.semibold)
                        Spacer()
                    }
                    HStack(spacing: 0){
                        Text(item?.name ?? "N/A")
                        Spacer()
                    }
                }
                .frame(width: (metrics.size.width - self.pad * 10) * 0.35)
                .padding(.vertical, 4)
                .padding(.leading, self.pad * 2)
                VStack(alignment: .leading){
                    HStack(spacing: 0){
                        Text(NSLocalizedString("YEAR", comment: "User year"))
                            .fontWeight(.semibold)
                        Spacer()
                    }
                    HStack(spacing: 0){
                        Text(item?.birthDateStr ?? "N/A")
                        Spacer()
                    }
                }
                .frame(width: (metrics.size.width - self.pad * 10) * 0.4)
                .padding(.vertical, 4)
                .padding(.leading, self.pad * 2)
                VStack(alignment: .leading){
                    HStack(spacing: 0){
                        Text(NSLocalizedString("AGE", comment: "User Age"))
                            .fontWeight(.semibold)
                        Spacer()
                    }
                    HStack(spacing: 0){
                        Text(item?.age != nil ? "\(item!.age)": "N/A")
                        Spacer()
                    }
                }
                .frame(width: (metrics.size.width - self.pad * 10) * 0.2)
                .padding(.vertical, 4)
                .padding(.leading, self.pad * 2)
                
                GeometryReader { metrics in
                    VStack(alignment: .leading, spacing: 0){
                        Button(action: {}, label: {
                            Image("trash")
                                .resizable()
                                .foregroundColor(.black)
                                .padding(.top, 4)
                                .padding(.trailing, 7)
                                .padding(.bottom, 3)
                        })
                        .frame(width: metrics.size.width, height: metrics.size.height / 2, alignment: .center)
                        .onTapGesture {
                            self.showDeleteAlert = true;
                            
//                            parentBindings.showLoader = true;
//                            APIController.delete(from: EndPoints.user, id: item?.id ?? 0) { (result) in
//                                self.parentBindings.showLoader = false;
//
//                                switch result {
//                                case .success(_):
//                                    self.modifiedCallback();
//                                case let .failure(error):
//                                    print(error);
//                                }
//                            }
                        }
                        .alert(isPresented: $showDeleteAlert){
                            Alert(
                                title: Text(LocalizedStringKey("ATENTION")),
                                message: Text(LocalizedStringKey("CONFIRM_DELETE")),
                                primaryButton: .cancel(Text(LocalizedStringKey("CANCEL"))),
                                secondaryButton: .default(Text(LocalizedStringKey("DELETE")), action: {
                                    self.parentBindings.showLoader = true;
                                    APIController.delete(from: EndPoints.user, id: item?.id ?? 0) { (result) in
                                        self.parentBindings.showLoader = false;
                                        
                                        switch result {
                                        case .success(_):
                                            self.modifiedCallback();
                                        case let .failure(error):
                                            print(error);
                                        }
                                    }
                                })
                                )
                        }
                        
                        Button(action: {}, label: {
                            Image("edit")
                                .resizable()
                                .foregroundColor(.black)
                                .padding(.bottom, 4)
                                .padding(.trailing, 7)
                                .padding(.top, 2)
                        })
                        .frame(width: metrics.size.width, height: metrics.size.height / 2, alignment: .center)
                        .onTapGesture{
                            parentBindings.selectedUser = item;
                            parentBindings.showPopup = true;
                        }
                    }
                }
                .frame(width: (metrics.size.width - self.pad * 10) * 0.1)
                .padding(0)
            }
            .background(Color.white)
            .clipShape(RoundedRectangle(cornerRadius: 6))
            .overlay(
                RoundedRectangle(cornerRadius: 6)
                    .stroke(Color.black, lineWidth: 2)
            )
            .padding(.horizontal, self.pad)
            .shadow(color: Color.black.opacity(0.3),
                    radius: 3,
                    x: 3,
                    y: 3)
        }
        .frame(height: 50)
    }
}

//class FilterSettings{
//    var filter: Filter = Filter()
//
//    private var _bindingName: String = ""
//
//    let bindingName = Binding<String>(get: {
//        return _bindingName
//    }, set: {
//        _bindingName = $0
//    })
//}

class CRUDViewBindings: ObservableObject {
    
    @Published var showPopup: Bool = false
    @Published var showLoader: Bool = false
    @Published var showFilters: Bool = false
    @Published var selectedUser: User?
    @Published var newUser: User = User()
    
}

struct CRUDView_Previews: PreviewProvider {
    
    static var previews: some View {
        Group {
            CRUDView()
                .environmentObject(CRUDViewBindings())
        }
    }
}
