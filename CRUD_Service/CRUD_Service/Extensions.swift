//
//  Extensions.swift
//  CRUD_Service
//
//  Created by Daniel Abad Jaraute on 13/02/2022.
//

import SwiftUI

extension Date{
    func yearsToday() -> Int {

        let calendar = Calendar.current

        let components = calendar.dateComponents([.year], from: self, to: Date())

        return components.year!
    }
}

extension View {
    func hideKeyboard() {
            UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
        }
}
