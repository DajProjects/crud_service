//
//  CRUD_ServiceApp.swift
//  CRUD_Service
//
//  Created by Daniel Abad Jaraute on 11/02/2022.
//

import SwiftUI

@main
struct CRUD_ServiceApp: App {
    @SwiftUI.Environment(\.scenePhase) var scenePhase;
    
    init(){
        // COnfiguración inicial al lanzar la app
    }
    
    var body: some Scene {
        WindowGroup {
            //ContentView()
            CRUDView()
                .environmentObject(CRUDViewBindings())
                .onOpenURL(perform: { url in
                    print("Incoming url: \(url)")
                })
        }
        // Gestión de los cambios de estado
        .onChange(of: scenePhase){(newScenePhase) in
            switch(newScenePhase){
            case .background:
                print("App State: Background")
            case .inactive:
                print("App State: Inactive")
            case .active:
                print("App State: Active")
            @unknown default:
                print("App State: Unknown")
            }
        }
    }
}
