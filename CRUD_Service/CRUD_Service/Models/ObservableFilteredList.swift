//
//  ObservableFilteredList.swift
//  CRUD_Service
//
//  Created by Daniel Abad Jaraute on 13/02/2022.
//

import Foundation

class ObservableFilteredList<T>: ObservableObject {
    
    private var list: [T] = [];
    @Published var filteredList: [T] = [];
    
    func filter(filter: Filter?){
        var result = self.list;
        
        if let f = filter{
            result = f.filterArray(list: result);
        }
        
        // Asignamos la lista filtrada al observable
        DispatchQueue.main.async {
            self.filteredList = result
        }
    }
    
    func setList(list: [T], filter: Filter?){
        self.list = list;
        self.filter(filter: filter);
    }
}
