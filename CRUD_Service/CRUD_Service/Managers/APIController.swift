//
//  APIManager.swift
//  CRUD_Service
//
//  Created by Daniel Abad Jaraute on 11/02/2022.
//

import Foundation
import Alamofire
import SwiftyJSON

struct ApiModel<T: Decodable>: Decodable{
    var data: [T];
    
    enum CodingKeys: String, CodingKey {
        case data = ""
    }
}


class APIController{
    
    static func get<T: Decodable>(from endpoint: String, type: T.Type, completion: @escaping (Swift.Result<[T], Error>)-> Void){
        
        let url = Environment.rootApiURL.appendingPathComponent(endpoint);
        
        AF.request(url, method: .get, parameters: nil)
            //.validate()
            .responseData(completionHandler: { (response) in
                switch response.result {
                case let .success(data):
                        do {
//                            let dateFormatter = DateFormatter()
//                            dateFormatter.dateFormat = "yyyy-MM-ddTHH:mm:ss"
//
//                            let decoder = JSONDecoder()
//                            decoder.dateDecodingStrategy = .formatted(dateFormatter)
                            
                            let result = try JSONDecoder().decode([T].self, from: data)
                            // success
                            completion(.success(result))
                        } catch {
                            // error
                            completion(.failure(error))
                        }
                    case let .failure(error):
                        // error
                        completion(.failure(error))
                }
            })
//            .responseDecodable(of: [T].self, completionHandler: { (response) in
//                print(response)
//                //completion(.success(response.value))
//            })
    }
    
    static func post<T: Encodable>(from endpoint: String, item: T, completion: @escaping (Swift.Result<Bool, Error>)-> Void){
        
        let url = Environment.rootApiURL.appendingPathComponent(endpoint);
        
        AF.request(url, method: .post, parameters: item, encoder: JSONParameterEncoder.default)
            .validate()
            .response(completionHandler: { (response) in
                switch response.result {
                case .success(_):
                    completion(.success(true))
                    case let .failure(error):
                        completion(.failure(error))
                }
            })
//            .responseData(completionHandler: { (response) in
//                switch response.result {
//                case .success(_):
//                    completion(.success(true))
//                    case let .failure(error):
//                        completion(.failure(error))
//                }
//            })
    }
    
    static func put<T: Encodable>(from endpoint: String, item: T, completion: @escaping (Swift.Result<Bool, Error>)-> Void){
        
        let url = Environment.rootApiURL.appendingPathComponent(endpoint);
        
        AF.request(url, method: .put, parameters: item, encoder: JSONParameterEncoder.default)
            .validate()
            .response(completionHandler: { (response) in
                switch response.result {
                case .success(_):
                    completion(.success(true))
                    case let .failure(error):
                        completion(.failure(error))
                }
            })
    }
    
    static func delete(from endpoint: String, id: Int, completion: @escaping (Swift.Result<Bool, Error>)-> Void){
        
        let url = Environment.rootApiURL.appendingPathComponent(endpoint + "/\(id)");
        
        AF.request(url, method: .delete, parameters: nil)
            .validate()
            .response(completionHandler: { (response) in
                switch response.result {
                case .success(_):
                    completion(.success(true))
                    case let .failure(error):
                        completion(.failure(error))
                }
            })
    }
}
