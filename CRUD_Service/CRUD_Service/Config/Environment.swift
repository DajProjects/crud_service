//
//  Environment.swift
//  CRUD_Service
//
//  Created by Daniel Abad Jaraute on 11/02/2022.
//

import Foundation

public enum Environment {
  private static let infoDictionary: [String: Any] = {
    guard let dict = Bundle.main.infoDictionary else {
      fatalError("Plist file not found")
    }
    return dict
  }()

  static let rootApiURL: URL = {
    guard let rootApiURLstring = Environment.infoDictionary["ROOT_URL"] as? String else {
      fatalError("Root URL not set in plist for this environment")
    }
    guard let url = URL(string: rootApiURLstring) else {
      fatalError("Root URL is invalid")
    }
    return url
  }()
}

public struct EndPoints{
    private static var api: String { return "/api" }
    static var user: String { return "\(api)/User"}
}

