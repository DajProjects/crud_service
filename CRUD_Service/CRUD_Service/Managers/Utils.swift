//
//  Utils.swift
//  CRUD_Service
//
//  Created by Daniel Abad Jaraute on 15/02/2022.
//

import Foundation

class Utils {
    
    static func getCustomDateF(_ format: String = "dd-MM-yyyy") -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format //"yyyy-MM-dd'T'12:00:00"
        
        return dateFormatter;
    }
}
