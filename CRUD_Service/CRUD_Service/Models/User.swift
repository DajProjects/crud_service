//
//  User.swift
//  CRUD_Service
//
//  Created by Daniel Abad Jaraute on 13/02/2022.
//

import Foundation

struct User: Codable, Identifiable{
    var id: Int
    var name: String
    var birthdate: String
    var birthDate: Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        return dateFormatter.date(from: String(birthdate.prefix(19))) ?? Date()
    }
    var age: Int {
        birthDate.yearsToday()
    }
    var birthDateStr: String {
        
        return Utils.getCustomDateF().string(from: birthDate)
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case birthdate
    }
}

extension User{
    init(){
        id = 0;
        name = "";
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'12:00:00"
        
        birthdate = dateFormatter.string(from: Date())
    }
}
