//
//  LoaderView.swift
//  CRUD_Service
//
//  Created by Daniel Abad Jaraute on 15/02/2022.
//

import SwiftUI

struct LoaderView: View {
    @EnvironmentObject var env : CRUDViewBindings
    
    var body: some View {
        GeometryReader { geometry in
            if env.showLoader {
                ZStack{
                    Color.black.opacity(env.showLoader ? 0.3 : 0)
                        .edgesIgnoringSafeArea(.all)
                    ProgressView(LocalizedStringKey("LOADING"))
                        .progressViewStyle(RingProgressViewStyle())
                }
                
                .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            }
        }
    }
}

public struct RingProgressViewStyle: ProgressViewStyle {
  private let defaultSize: CGFloat = 120
  private let lineWidth: CGFloat = 12
  private let defaultProgress = 0.2 // CHANGE

  // tracks the rotation angle for the indefinite progress bar
  @State private var fillRotationAngle = Angle.degrees(-90) // ADD

  public func makeBody(configuration: ProgressViewStyleConfiguration) -> some View {
    VStack {
        HStack{
            Spacer()
            configuration.label
            .font(.title)
            .foregroundColor(Color.white)
                .padding(8)
            Spacer()
        }
        
      progressCircleView(fractionCompleted: configuration.fractionCompleted ?? defaultProgress,
               isIndefinite: configuration.fractionCompleted == nil) // UPDATE
      configuration.currentValueLabel
        Spacer()
    }
    .background(Color.black.opacity(0.6))
    .frame(width: 200, height: 200)
    .clipShape(RoundedRectangle(cornerRadius: 60))
  }

  private func progressCircleView(fractionCompleted: Double,
                              isIndefinite: Bool) -> some View {
     // this is the circular "track", which is a full circle at all times
    Circle()
      .strokeBorder(Color.white.opacity(0.5), lineWidth: lineWidth, antialiased: true)
      .overlay(fillView(fractionCompleted: fractionCompleted, isIndefinite: isIndefinite)) // UPDATE
      .frame(width: defaultSize, height: defaultSize)
  }

  private func fillView(fractionCompleted: Double,
                              isIndefinite: Bool) -> some View { 
    Circle() // the fill view is also a circle
      .trim(from: 0, to: CGFloat(fractionCompleted))
      .stroke(Color.blue, lineWidth: lineWidth)
      .frame(width: defaultSize - lineWidth, height: defaultSize - lineWidth)
      .rotationEffect(fillRotationAngle) // UPDATE
      // triggers the infinite rotation animation for indefinite progress views
      .onAppear {
        if isIndefinite {
            withAnimation(Animation.easeInOut(duration: 1.0).repeatForever()){
                fillRotationAngle = .degrees(270)
            }
        }
      }
  }
}

struct LoaderView_Previews: PreviewProvider {
    
    static var previews: some View {
        LoaderView()
    }
}
