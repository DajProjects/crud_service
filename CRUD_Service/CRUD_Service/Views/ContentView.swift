//
//  ContentView.swift
//  CRUD_Service
//
//  Created by Daniel Abad Jaraute on 11/02/2022.
//

import SwiftUI

struct ContentView: View {
    
    init(){
        
    }
    
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
