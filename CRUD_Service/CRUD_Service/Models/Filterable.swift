//
//  Filterable.swift
//  CRUD_Service
//
//  Created by Daniel Abad Jaraute on 13/02/2022.
//

import SwiftUI

class Filter {
    var filters: [FilterElem] = []
    
    func addFilter(name: String, mode: FilterMode, value: Any){
        
        if filters.contains(where: { (f) -> Bool in
            f.name == name
        }){
            editFilter(name: name, mode: mode, value: value)
        }
        else
        {
            filters.append(FilterElem(mode: mode, name: name, val: value))
        }
    }
    
    private func editFilter(name: String, mode: FilterMode, value: Any){
        if var f = filters.first(where: { (fi) -> Bool in
            fi.name == name
        }){
            f.mode = mode;
            f.val = value;
        }
    }
    
    func getFilter(name: String) -> FilterElem?{
        return filters.first(where: { (fi) -> Bool in
            fi.name == name
        })
    }
    
    func removeFilter(name: String){
        filters.removeAll { (f) -> Bool in
            f.name == name
        }
    }
    
    func filterArray<T>(list: [T]) -> [T]{
        var result = list;
        
        for f in filters {
            result = result.filter({ (i) -> Bool in
                let mirror = Mirror(reflecting: i);
                
                var isValid = true;
                if let childProp = mirror.children.first(where: { (lab, val) -> Bool in
                    return lab == f.name
                }){
                    isValid = filterItem(f, childProp)
                }
                
                return isValid;
            })
        }
        
        return result;
    }
    
    private func filterItem(_ f: FilterElem, _ item: Mirror.Child) -> Bool {
        var valid = false;
        
        // Int
        if let a = item.value as? Int, let b = f.val as? Int {
            valid = (f.mode == .Equal && a == b) ||
                (f.mode == .Greater && a > b) ||
                (f.mode == .Lower && a < b)
        }
        // String
        else if let a = item.value as? String, let b = f.val as? String {
            valid = (f.mode == .Equal && a.lowercased() == b.lowercased()) ||
                (f.mode == .StartWith && a.starts(with: b)) ||
                (f.mode == .EndWith && a.hasSuffix(b)) ||
                (f.mode == .Contains && a.contains(b))
        }
        // Date
        else if let a = item.value as? Date, let b = f.val as? Date {
            valid = (f.mode == .Equal && a == b) ||
                (f.mode == .Greater && a > b) ||
                (f.mode == .Lower && a < b)
        }
        
        return valid
    }
}

enum FilterMode: String {
    case Contains = "FM_CONTAINS"
    case StartWith = "FM_START_WITH"
    case EndWith = "FM_END_WITH"
    case Equal = "FM_EQUAL"
    case Lower = "FM_LOWER"
    case Greater = "FM_GREATER"
}

struct FilterElem {
    var mode: FilterMode
    var name: String
    var val: Any
    
//    static private func getModes(_ a: [FilterMode])->Dictionary<String, FilterMode>{
//        var result : Dictionary<String, FilterMode> = [:]
//
//        for m in a {
//            result[NSLocalizedString(m.rawValue, comment: "")] = m
//        }
//
//        return result;
//    }
    
    static func getNumericModes()->[FilterMode]{
        return [FilterMode.Equal, FilterMode.Lower, FilterMode.Greater];
    }
    
    static func getStringModes()->[FilterMode]{
        return [FilterMode.Contains, FilterMode.Equal, FilterMode.StartWith, FilterMode.EndWith];
    }
}
